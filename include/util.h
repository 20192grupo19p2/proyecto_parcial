
#ifndef UTIL_H
#define UTIL_H
#include "vida.h"

/*
Funcion que dibuja una grilla en la terminal.
Recibe un arreglo 2D de chars. Si el elemento es el 
numero 0, se muestra un espacio vacio, si es 1, se 
muestra una x.

Cada llamada de esta funcion borrara todo lo mostrado
en pantalla.

*/
void dibujar_grilla(char **matriz, int fil, int col);


/*
Llena matriz con cantidad de 1s en posiciones al azar,
el resto lo llena de 0s.
*/
void llenar_matriz_azar(char **grilla, int fil, int col, int cantidad);


void begin(int filas, int columnas, int celulas);

char ** reglas(char **simulacion, int filas, int columnas);

char **simulacion(int filas, int columnas);

void estadisticas(juego_de_vida juego, long *vivasT, long *muertasT, long generacion);
void validaciones(juego_de_vida *juego);
void evitar_Memory(char **simulacion, int filas);


#endif 
