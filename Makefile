
bin/vida: obj/util.o obj/main.o
	gcc -Wall -g obj/util.o obj/main.o -o bin/vida -fsanitize=address,undefined

obj/util.o: src/util.c include/util.h
	gcc -Wall -g -c -I include/ src/util.c -o obj/util.o -fsanitize=address,undefined

obj/main.o: src/main.c 
	gcc -Wall -g -c -I include/ src/main.c -o obj/main.o -fsanitize=address,undefined

.PHONY: clean

clean:
	rm -rf obj bin
	mkdir obj bin
