#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <util.h>
#include <vida.h>

//struct juego_de_vida juego;

void dibujar_grilla(char **matriz, int fil, int col){		
	printf("\e[1;1H\e[2J");
	char *linea = malloc(col + 1);	//Char nulo al final
	for(int i = 0; i < fil; i++){
		memset(linea, ' ', col+1);
		linea[col] = 0;
		for(int j = 0; j < col; j++){
			if(matriz[i][j] == 0){
				linea[j] = ' ';
			}
			else if(matriz[i][j] == 1){
				linea[j] = 'x';			
			}
		}
		printf("%s", linea);
		printf("\n");
		fflush(stdout);
	}

}

void evitar_Memory(char **simulacion, int filas)
{
		for(int i = 0; i < filas; i++){
			        free(simulacion + i);
				    }
		    free(simulacion);
}


void validaciones(juego_de_vida *juego)
{
	    if((juego -> filas == 0) || (juego -> columnas == 0)){
		            printf("El tablero no puede ser creado con 0 filas o 0 columnas. \nSaliendo del programa\n");
			            exit(0);
				     	}
	        if((juego -> filas) < 0){
			        printf("El numero de filas ingresado es menor a cero, se cambia a positivo para que pueda ingresar\n");
				        juego -> filas = labs(juego -> filas);
					    }
		    if((juego -> columnas) < 0){
			            printf("El numero de columnas ingresado es menor a cero, se cambia a positivo para que pueda ingresar\n");
				            juego -> columnas = labs(juego -> columnas);
					        }
		        if((juego -> tiempo_sleep) < 0){
				        printf("El tiempo de retardo es menor a cero, se cambiara a un valor positivo para que pueda ingresar\n");
					        juego -> tiempo_sleep = labs(juego -> tiempo_sleep);
						    }
			    printf("Presione ENTER para continuar\n");
			        getchar();
}


void estadisticas(juego_de_vida juego, long *vivasT, long *muertasT, long generacion)
{
       	long cVivas = 0;
      	long cMuertas = 0;
	for(long i = 0; i < juego.filas; i++){
		for(long j = 0; j < juego.columnas; j++){
			if(juego.tablero[i][j] == 1){
				cVivas++;
			}
		}
	}
	cMuertas = (juego.filas * juego.columnas) - cVivas;
	*vivasT += cVivas;
	*muertasT += cMuertas;
	long double promedio_cv = (*vivasT) / generacion;
	long double promedio_cm = (*muertasT) / generacion;

	printf("Generacion: %li\n", generacion);
	printf("\tNumero de celulas vivas en esta generacion: %li\n", cVivas);
	printf("\tNumero de celulas muertas en esta generacion: %li\n", cMuertas);
	printf("\tNumero de celulas vivas desde generacion 0: %li\n", *vivasT);
	printf("\tNumero de celulas muertas desde generacion 0: %li\n", *muertasT);
	printf("\tPromedio de celulas que murieron por generacion: %.2Lf\n", promedio_cm);
	printf("\tPromedio de celulas que nacieron por generacion: %.2Lf\n", promedio_cv);
}

void llenar_matriz_azar(char **grilla, int fil, int col, int cantidad){
	for(int i =0; i<fil; i++){
		memset(grilla[i], 0, col);	
	}
	srand(time(NULL));
	for(int i = 0; i < cantidad; i++){
		long rnd_fil = rand() % fil;	
		long rnd_col = rand() % col;
		
		//printf("%ld %ld\n", rnd_fil, rnd_col);
		if(grilla[rnd_fil][rnd_col] == 1){
			i--;
		}
		else{
			grilla[rnd_fil][rnd_col] = 1;
		}
		
	}
}

char **crearSimulacion(int filas, int columnas)
{
	char **simulacion = (char **) malloc(filas*sizeof(char *));
	if(simulacion == NULL){
		exit(0);
	}
	for(int i = 0; i < filas; i++){
		*(simulacion + i) = (char *)malloc(columnas*sizeof(char));
	}
	//Llenado del arreglo
	/*for (int x = 0; x < filas; x++){
		for(int y = 0; y < columnas; y++){
			simulacion[x][y] = '0';
		}
	}*/
	return simulacion;
}

/*
void begin (int filas, int columnas, int celulas){
	printf("Inicio tablero\n");
	char **simulacion = crearSimulacion(filas, columnas);
	llenar_matriz_azar(simulacion, filas, columnas,celulas);
	dibujar_grilla(simulacion, filas, columnas);
	printf("Final del tablero\n");
	reglas(simulacion, filas, columnas);
}
*/

char ** reglas(char **simulacion, int filas, int columnas)
{
	int vivos = 0;
	char **nextGen = (char **)malloc(filas*sizeof(char *));
	for(int r = 0; r < filas; r++){
		*(nextGen + r) = (char *)malloc(columnas * sizeof(char));
	}
	if(nextGen == NULL){
		printf("NULL\n");
		exit(0);
	}
	//No tomo en cuenta las esquinas ni los bordes
	for(int i = 1; i < filas -1; i++){                             
		for(int j = 1; j < columnas -1; j++){
			for(int x = -1; x <= 1; x++){
				for(int y = -1; y <= 1; y++){
					if(*(*(simulacion+(i+x)) + (j+y)) == 1){
						vivos++;
					}
				}
			}vivos--;

			//Aplicacion de reglas
			if((simulacion[i][j] == 1) && vivos <= 1){		
				//Muerte por soledad
				nextGen[i][j] = 0;
			}else if((simulacion[i][j] == 1) && vivos >= 4){
				//Muerte por sobrepoblacion
				nextGen[i][j] =0;
			}else if((simulacion[i][j] == 0) && vivos == 3){
				//Creacion ya que tengo 3 vecinos
				nextGen[i][j] = 1;
			}else if((simulacion[i][j] == 1) && (vivos == 2 || vivos == 3)){
				//Sobrevive la celula
				nextGen[i][j] = 1;
			}/*else if(simulacion[i][j] == 0){
				//Cualquier otro caso, la celula permanece vacia
				nextGen[i][j] = 0;
			}*/
			else{
				nextGen[i][j] = simulacion[i][j];
			}
		}
	}
//	dibujar_grilla(nextGen, filas, columnas);
	return nextGen;
}
/*	 int cv=0;
	 //Esquina superior izquierda
	 if (simulacion[0][0]==1 || simulacion[0][0] ==0 ){
		 if(simulacion[1][0]==1){	//abajo
			 cv++;
		 }
		 if(simulacion[0][1]==1){	//alado-derecha
			 cv++;
		 }
		 if(simulacion[1][1]==1){	//abajo-derecha
			 cv++;
		 }
		 if(simulacion[0][0]==1){	//condicion viva
			 if(cv>=4 || cv <=1){
				 nextGen[0][0]=0;
			 }else if(cv==2 || cv==3){
				 nextGen[0][0]=1;
			 }
		 }
		 else if(simulacion[0][0]=0){	//condicion muerta
			 if(cv==3){
				 nextGen[0][0]=1;
			 }
		 }
		 cv=0;
	 }

	 //Esquina superior derecha
	 if(simulacion[0][columnas-1]==1 || simulacion[0][columnas-1]==0){
		 if(simulacion[0][columnas-2]==1){	//alado izquierda
			 cv++;
		 }
		 if(simulacion[1][columnas-2]==1){	//abajo-izquierdo
			 cv++;
		 }
		 if(simulacion[1][columnas-1]==1){	//abajo
			 cv++;
		 }
		 if(simulacion[0][columnas-1]==1){	//condicion viva
			 if(cv>=4 || cv <=1){
				 nextGen[0][columnas-1]=0;
			 }else if(cv==2 || cv==3){
				 nextGen[0][columnas-1]=1;
			 }
		 }
		 else if(simulacion[0][columnas-1]==0){	//condicion muerta
			 if(cv==3){
				 nextGen[0][columnas-1]=1;
			 }
		 }
		 cv = 0;
	 }
	 //Esquina inferior izquierda
	 if(simulacion[filas-1][0]==1 || simulacion[filas-1][0]==0){
		 if(simulacion[filas-2][0]==1){		//arriba
			 cv++;
		 }
		 if(simulacion[filas-2][1]==1){		//arriba-derecha
			 cv++;
		 }
		 if(simulacion[filas-1][1]==1){		//alado-derecha
			 cv++;
		 }
		 if(simulacion[filas-1][0]==1){		//condicion Viva
			 if(cv>=4 || cv<=1){
				 nextGen[filas-1][0]=0;
			 }else if(cv==2 || cv==3){
				 nextGen[filas-1][0]=1;
			 }
		 }
		 else if(simulacion[filas-1][0]==0){	//condicion Muerta
			 if(cv==3){
				 nextGen[filas-1][0]=1;
			 }
		 }
		 cv=0;
	 }

	 //Esquina inferior derecha
	 if(simulacion[filas-1][columnas-1]==1 || simulacion[filas-1][columnas-1]==0){
		 if(simulacion[filas-2][columnas-1]==1){	//arriba
			 cv++;
		 }
		 if(simulacion[filas-2][columnas-2]==1){	//arriba-izquierda
			 cv++;
		 }
		 if(simulacion[filas-1][columnas-2]==1){	//alado-izquierda
			 cv++;
		 }
		 if(simulacion[filas-1][columnas-1]==1){	//condicion Viva
			 if(cv>4 || cv<=1){
				 nextGen[filas-1][columnas-1]=0;
			 }else if(cv==2 ||cv==3){
				 nextGen[filas-1][filas-1]=1;
			 }
		 }
		 else if(simulacion[filas-1][columnas-1]==0){	//condicion muerta
			 if(cv==3){
				 nextGen[filas-1][columnas-1]=1;
			 }
		 }
		 cv=0;
	 }

	//Columna[0] sin la esquina [0][0] ni la esquina [filas-1][0]
	for(int i=1; i<filas-1; i++){
		if (simulacion[i][0]==1 || simulacion[i][0]==0){
			if(simulacion[i-1][0]==1){	//arriba
				cv++;
			}
			if(simulacion[i-1][0]==1){	//abajo
				cv++;
			}
			if(simulacion[i-1][1]==1){	//arriba-derecha
				cv++;
			}
			if(simulacion[i+1][1]==1){	//ababjo-derecha
				cv++;
			}
			if(simulacion[i][1]==1){	//alado-derecha
				cv++;
			}
			if(simulacion[i][0]==1){	//condicion Viva
				if(cv>4 || cv<=1){
					nextGen[i][0]=0;
				}else if(cv==2 || cv ==3){
					nextGen[i][0]=1;
				}
			}else if(simulacion[i][0]==0){	//condicion muerta
				if(cv==3){
					nextGen[i][0]=1;
				}
			}
		}
		cv=0;
	}	

	
	//columna[columnas-1] sin las esquinas
	for(int i=1; i<filas-1; i++){
		if(simulacion[i][columnas-1]==1 || simulacion[i][columnas-1]==0){
			if(simulacion[i-1][columnas-1]==1){	//arriba
				cv++;
			}
			if(simulacion[i+1][columnas-1]==1){	//abajo
				cv++;
			}
			if(simulacion[i-1][columnas-2]==1){	//arriba-izquierda
				cv++;
			}
			if(simulacion[i+1][columnas-2]==1){	//abajo-izquierda
				cv++;
			}
			if(simulacion[i][columnas-2]==1){	//alado-izquierda
				cv++;
			}
			if(simulacion[i][columnas-1]==1){	//condicion Viva
				if(cv>4 || cv <=1){
					nextGen[i][columnas-1]=0;
				}else if(cv==2 || cv==3){
					nextGen[i][columnas-1]=1;
				}
			}else if(simulacion[i][columnas-1]==0){		//condicion muerta
				if(cv==3){
					nextGen[i][columnas-1]=1;
				}
			}
		}
		cv=0;
	}

	//fila[0] sin las esquinas
	for(int i=1; i<columnas-1; i++){
		if(simulacion[0][i] || simulacion[0][i]==0){
			if(simulacion[1][i]==1){	//abajo
				cv++;
			}
			if(simulacion[1][i-1]==1){	//abajo-izquierda
				cv++;
			}
			if(simulacion[1][i+1]==1){	//abajo-derecha
				cv++;
			}
			if(simulacion[0][i-1]==1){	//izquierda
				cv++;
			}
			if(simulacion[0][i+1]==1){	//derecha
				cv++;
			}
			if(simulacion[0][i]==1){	//condicion viva
				if(cv>4 || cv<=1){
					nextGen[0][i]=0;
				}else if(cv==2 || cv==3){
					nextGen[0][i]=1;
				}
			}else if(simulacion[0][i]==0){
				if(cv==3){
					nextGen[0][i]=1;
				}
			}
		}
		cv=0;
	}

	//fila[filas-1] sin las esquinas
	for(int i=1; i<columnas-1; i++){
		if(simulacion[filas-1][i]==1 || simulacion[filas-1][i]==0){
			if(simulacion[filas-2][i]==1){		//arriba
				cv++;
			}
			if(simulacion[filas-2][i-1]==1){	//arriba-izquierda
				cv++;
			}
			if(simulacion[filas-2][i+1]==1){	//arriba-derecha
				cv++;
			}
			if(simulacion[filas-1][i-1]==1){	//izquierda
				cv++;
			}
			if(simulacion[filas-1][i+1]==1){	//derecha
				cv++;
			}
			if(simulacion[filas-1][i]==1){		//condicion viva
				if(cv>4 || cv<=1){
					nextGen[filas-1][i]=0;
				}else if(cv==2 || cv==3){
					nextGeb[filas-1][i]=1;
				}
			} else if(simulacion[filas-1][i]==0){	//condicion muerta
				if(cv==3){
					nextGen[filas-1][i]=1;
				}
			}
		}
		cv=0;
	}	


	* /	 //dibujar_grilla(nextGen, filas, columnas);

//}





