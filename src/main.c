#include <stdio.h>
#include "util.h"
#include "vida.h"
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

char **crearSimulacion(int filas, int columnas);

int main(int argc, char **argv){
	
	int opcion = 0;	
	//Estructura
	juego_de_vida juego;
	juego_de_vida copia;

	long celula;
	long celulas_vivas_generacion0 = 0;
	long celulas_muertas_generacion0 = 0;
	//char **simulacion;

	while ((opcion= getopt(argc, argv, "f:c:g:s:i:"))!= -1){
		switch(opcion){

			case 'f':
				juego.filas = atol(optarg);
				break;
			case 'c':				
				juego.columnas = atol(optarg);
				break;
			case 'g':
				juego.generaciones = atol(optarg);
				break;
			case 's':				
				juego.tiempo_sleep = atol(optarg);
				break;
			case 'i':
				celula = atol(optarg);
				break;
			default:
				printf("Argumento %c invalido\n", opcion);
				break;	
			
		}
	}

	/*Validacion*/
	if(juego.filas <= 0 || juego.columnas <= 0 || juego.tiempo_sleep <= 0){
		//exit(0);
		validaciones(&juego);
	}
	
	if(celula < 0){
		printf("El numero de celulas vivas no puede ser menor a cero, se procedera a cambiar a su correspondiente valor positivo\n");
		celula = labs(celula);
		printf("ENTER para continuar\n");
		getchar();
	}
	
	juego.tablero = crearSimulacion(juego.filas, juego.columnas);
	llenar_matriz_azar(juego.tablero, juego.filas, juego.columnas, celula);
	dibujar_grilla(juego.tablero, juego.filas, juego.columnas);
	/*simulacion=reglas(juego.tablero, juego.filas, juego.columnas);
	copia.tablero = simulacion;*/
	int i = 0;
	if(juego.generaciones <= 0){
		while(1){
			llenar_matriz_azar(juego.tablero, juego.filas, juego.columnas, celula);
			dibujar_grilla(juego.tablero, juego.filas, juego.columnas);
			estadisticas(juego, &celulas_vivas_generacion0, &celulas_vivas_generacion0, i+1);
			usleep(juego.tiempo_sleep);
			i++;
		}
	}else{
		for(i = 0; i < juego.generaciones; i++){
			printf("Inicio\n");
			//llenar_matriz_azar(juego.tablero, juego.filas, juego.columnas, celula);
			copia.tablero = reglas(juego.tablero, juego.filas, juego.columnas);
			dibujar_grilla(copia.tablero, juego.filas, juego.columnas);
			estadisticas(copia, &celulas_vivas_generacion0, &celulas_muertas_generacion0,i+1);
			usleep(juego.tiempo_sleep);
			//copia.tablero = reglas(copia.tablero, juego.filas, juego.columnas);
			printf("Final\n");
		}
	}
	evitar_Memory(copia.tablero, juego.filas);
	/*
	juego.tablero = crearSimulacion(juego.filas, juego.columnas);
	llenar_matriz_azar(juego.tablero, juego.filas, juego.columnas, celula);
	dibujar_grilla(juego.tablero, juego.filas, juego.columnas);*/
	evitar_Memory(juego.tablero, juego.filas);
	printf("Final\n");

	return 0;
}
